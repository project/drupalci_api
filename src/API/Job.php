<?php

namespace API;

use Symfony\Component\HttpFoundation\Request;

class Job implements \JsonSerializable {

  protected $id = 0;
  protected $title;
  protected $jobType;
  protected $commit;
  protected $issue;
  protected $created;
  protected $repository;
  protected $branch;
  protected $patch;
  protected $status;

  /**
   * Human-readable results string. '10 passes, 2 fails, 1 exception.'
   * @var string
   */
  protected $result;

  /**
   * Tags for use by Results and d.o.
   * @var array
   */
  protected $tags;

  /**
   * Keyed array specifying test environment dependencies.
   *
   * Not currently used.
   * @var array
   *   - 'db': Database.
   *   - 'php': Version.
   */
  protected $environment;
  protected $dbversion;
  protected $phpversion;
  protected $jenkinsUri;

  /**
   * Construct a Job object.
   */
  public function __construct() {
    // We must put a DateTime object in some properties or Doctrine will
    // complain.
    $this->created = new \DateTime();
  }

  public static function jsonProperties() {
    return [
      'id',
      'title',
      'jobType',
      'status',
      'result',
      'repository',
      'branch',
      'commit',
      'issue',
      'patch',
      'tags',
      'environment',
      'phpversion',
      'dbversion',
    ];
  }

  public static function createFromRequest(Request $request) {
    $query = [];
    foreach (static::jsonProperties() as $query_key) {
      $query[$query_key] = $request->get($query_key, '');
    }
    // Sanity check.
    if (empty($query['title']) || empty($query['repository']) || empty($query['branch'])) {
      // @todo: Make a meaningful exception class.
      throw new \InvalidArgumentException('Job start requests need at least a job title, repository and a branch.');
    }
    $job = new static();
    $job->setId($query['id']);
    $job->setTitle($query['title']);
    $job->setJobType($query['jobType']);
    $job->setStatus($query['status']);
    $job->setResult($query['result']);
    $job->setRepository($query['repository']);
    $job->setBranch($query['branch']);
    $job->setCommit($query['commit']);
    $job->setIssue($query['issue']);
    $job->setPatch($query['patch']);
    $job->setTags((array)$query['tags']);
    $job->setEnvironment((array)$query['environment']);
    $job->setPhpVersion($query['phpversion']);
    $job->setDbVersion($query['dbversion']);
    return $job;
  }

  /**
   * Set the created date.
   */
  public function setCreated($created) {
    $this->created = $created;

    return $this;
  }

  /**
   * Get created date.
   *
   * @return \DateTime
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   * Set id.
   */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
   * Get id.
   *
   * @return integer
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set title.
   *
   * @return string
   */
  public function setTitle($title) {
    $this->title = $title;

    return $this;
  }

  /**
   * Get title.
   *
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  public function setJobType($job_type) {
    $this->jobType = $job_type;
    return $this;
  }

  public function getJobType() {
    return $this->jobType;
  }

  public function setCommit($commit) {
    $this->commit = $commit;
    return $this;
  }

  public function getCommit() {
    return $this->commit;
  }

  public function setIssue($issue) {
    $this->issue = $issue;
    return $this;
  }

  public function getIssue() {
    return $this->issue;
  }

  public function setTags(array $tags) {
    if (!is_array($tags)) {
      $tags = (array) $tags;
    }
    $this->tags = $tags;
    return $this;
  }

  public function getTags() {
    return $this->tags;
  }

  public function setEnvironment(array $environment) {
    if (!is_array($environment)) {
      $environment = (array) $environment;
    }
    $this->environment = $environment;
    return $this;
  }

  public function getEnvironment() {
    return $this->environment;
  }

  /**
   * Set Php Container Version
   *
   * @param string $phpversion
   * @return Job
   */
  public function setPhpVersion($phpversion) {
    $this->phpversion = $phpversion;

    return $this;
  }
  /**
   * Get php version.
   *
   * @return string
   */
  public function getPhpVersion() {
    return $this->phpversion;
  }

  /**
   * Set Database Container Version
   *
   * @param string $dbversion
   * @return Job
   */
  public function setDbVersion($dbversion) {
    $this->dbversion = $dbversion;

    return $this;
  }
  /**
   * Get Database Container version.
   *
   * @return string
   */
  public function getDbVersion() {
    return $this->dbversion;
  }

  /**
   * Set repository.
   *
   * @param string $repository
   * @return Job
   */
  public function setRepository($repository) {
    $this->repository = $repository;

    return $this;
  }

  /**
   * Get repository.
   *
   * @return string
   */
  public function getRepository() {
    return $this->repository;
  }

  /**
   * Set branch.
   *
   * @param string $branch
   * @return Job
   */
  public function setBranch($branch) {
    $this->branch = $branch;

    return $this;
  }

  /**
   * Get branch.
   *
   * @return string
   */
  public function getBranch() {
    return $this->branch;
  }

  /**
   * Set patch
   *
   * @param string $patch
   * @return Job
   */
  public function setPatch($patch) {
    $this->patch = $patch;

    return $this;
  }

  /**
   * Get patch
   *
   * @return string
   */
  public function getPatch() {
    return $this->patch;
  }

  /**
   * Set status
   *
   * @param string $status
   * @return Job
   */
  public function setStatus($status) {
    $this->status = $status;

    return $this;
  }

  /**
   * Get status
   *
   * @return string
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Set result
   *
   * @param string $result
   * @return Job
   */
  public function setResult($result) {
    $this->result = $result;

    return $this;
  }

  /**
   * Get result
   *
   * @return string
   */
  public function getResult() {
    return $this->result;
  }

  /**
   * Set Jenkins URI
   *
   * @param string $result
   * @return Job
   */
  public function setJenkinsUri($jenkinsUri) {
    $this->jenkinsUri = $jenkinsUri;

    return $this;
  }

  /**
   * Get Jenkins URI
   *
   * @return string
   */
  public function getJenkinsUri() {
    return $this->jenkinsUri;
  }

  public function jsonSerialize() {
    $result = new \stdClass();
    $properties = static::jsonProperties();
    foreach ($properties as $property) {
      $result->$property = $this->$property;
    }
    return $result;
  }

}
